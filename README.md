# gimp-test-images

The gimp-test-images repository is intended to be used for collecting test
images, to be used by GIMP's CI tests, to test that our file import and export
plug-ins do not cause any regressions.

Single file format test sets should be organized under a folder (lowercase)
named after the primary extension for the file format.
Test sets containing multiple file sets can be added at the root to make it
easier to update them. In case there are only a few file formats, or if it
isn't likely that the set will be updated, you could choose to split and
reference parts of the set under the respective file format folders.

## Single file format sets
* [bmpsuite](https://entropymine.com/jason/bmpsuite/) A collection of bmp test images.
  + [License](bmp/bmpsuite/COPYING.txt)
  + [Readme](bmp/bmpsuite/readme.txt)
* [pngsuite](http://www.schaik.com/pngsuite/) A collection of png test images.
  + [License](png/pngsuite/PngSuite.LICENSE)
  + [Readme](png/pngsuite/PngSuite.README)
* [brokensuite](http://code.google.com/p/javapng/) A collection of broken png images
to test error handling.
  + [Readme](png/brokensuite/readme.txt)
* [Imager-File-TIFF test images](https://metacpan.org/release/TONYC/Imager-File-TIFF-0.89/source)
A collection of tiff test images.
* [psd-tools](https://github.com/psd-tools/psd-tools) A collection of psd/psb
test images as part of a Python package for working with Photoshop files.
  + [License](https://github.com/psd-tools/psd-tools/blob/main/LICENSE)
  + [Readme](https://github.com/psd-tools/psd-tools/blob/main/README.rst)
* [psdparse](http://www.telegraphics.com.au/svn/psdparse/trunk/psd/) A collection
of psd test images as part of a psd reader. Note: link is broken.

## Image collections with multiple file formats
* [zaaIL TestImages](https://github.com/ZaaLabs/ZaaIL-TestImages) A collection of
a wide range of image file formats.
  + [Readme](zaail-testimages/README)
* [imagetestsuite](https://code.google.com/archive/p/imagetestsuite/) A collection
of fuzzed gif, jpg, png and tif images, each listed under the respective
extension. [Clone on github](https://github.com/Wormnest/imagetestsuite/)
  + [Wiki](https://github.com/Wormnest/imagetestsuite/wiki)
  + License: GNU GPL v2, Content License: Creative Commons 3.0 BY.
* [sembiance/telparia](https://sembiance.com/fileFormatSamples/image/) We use some
images from the telparia collection, which apparently recently got renamed to
sembiance. They are not listed here as a collection, but can be found under
the respective file formats.
* [Pillow](https://github.com/python-pillow/Pillow/tree/main) A collection of
test images from the Pillow image library.
  + [License](pillow/LICENSE)
* [GraphicEx](https://github.com/mike-lischke/GraphicEx/tree/master/TestSuite%20original)
A collection of test images from the graphicex library.
  + [License](https://github.com/mike-lischke/GraphicEx/blob/master/LICENSE)
* [OpenImageIO](https://github.com/AcademySoftwareFoundation/OpenImageIO-images/)
A collection of test images from the OpenImageIO library.
They are not listed here as a collection, but can be found under the respective
file formats in folders starting with `oiio`.
  + [Readme](https://github.com/AcademySoftwareFoundation/OpenImageIO-images/blob/master/README)
* [SerenityOS Test Images](https://github.com/SerenityOS/serenity/tree/master/Tests/LibGfx/test-inputs)
A collection of test images for SerenityOS's LibGfx tests. They are not listed here as a collection, 
but can be found under the respective file formats.
